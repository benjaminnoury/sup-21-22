---
titre: 
type: "**Dimensionnement des roulements d'un réducteur**"
classe: 
chapitre: 
question-section: 1
#document: twocolumn,landscape
---


\definecolor{shadecolor}{HTML}{DFDDEB}

Soit le réducteur à trains droits ci-dessous.

\image{img/plan.png}

Les engrenages ont un angle de contact de 20° 


# Cinématique

\question{Calculer le rapport de réduction du train. \emph{On pourra mesurer les diamètres primitif sur le plan}}


# Montage de roulements

\question{Quels sont les roulements utilisés ? Comment est réalisée la lubrification ?}
\question{Pour chaque montage, proposer un modèle du montage s'appuyant sur des liaisons parfaites.}
\question{Justifier les choix des arrêts axiaux au vue de la tenue aux efforts et de la montabilité.}
\question{Proposer un schéma cinématique équivalent du réducteur.}

\question{Expliquer comment est réalisé l'étanchéité}



\question{Calculer l'hyperstatisme. Identifier les dispositions constructives permettant de remédier à un éventuel hyperstatisme. \begin{shaded}{Les engrènements entre les pignons pourront être représentés par des liaisons linéaires annulaires}\end{shaded}
}

Le diamètre de l'arbre de sortie est de 20 mm.
