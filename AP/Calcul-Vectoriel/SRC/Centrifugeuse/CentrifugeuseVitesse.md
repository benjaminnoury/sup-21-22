---
titre: Centrifugeuse de laboratoire
type: TD
classe: ATS
CI: 7 Transmettre l'énergie - Mouvement
competence: 
#question-section: 1
#document: twocolumn,landscape
---

\begin{multicols}{2}
Les centrifugeuse de laboratoire sont utilisées pour séparer les différents composants d'un liquide. Une des principales caractéristiques de ce type de matériel est sa capacité d'accélération définie par le nombre de "g" qui représente l'accélération subie par le liquide.

Sous l'effet de cette accélération, les éléments dont la masse volumique est la plus grande se concentrent sur le fond de l'éprouvette.


La centrifugeuse étudiée est équipée de paniers oscillants. Lors de la rotation du rotor, les paniers s'inclinent, ce qui augmente l'effet centrifuge et donc l'efficacité de la machine.

Le constructeur indique une vitesse de rotation du rotor (1) réglable entre 400 tr/min et 10000 tr/min et une accélération maximale de 26000 g  avec g$\simeq$10m/s$^{2}$.

\image{img/centrifugeuse.png}
\end{multicols}



\begin{center}
	\begin{minipage}{.3\lw}
\begin{tikzpicture}
\node[draw, circle] (A) at (0,0) {1};
\node[draw, circle] (B) at (0,-2) {2};
\node[draw, circle] (C) at (0,-4) {3};

\draw (A)--(B) node[midway, left ]{Pivot d'axe $(O_{1}, \vec z_{1})$};
\draw (B)--(C) node[midway, left ]{Pivot d'axe $(O_{3}, \vec y_{3})$};

\draw (A)--++(.5,0)--++(0,.2)--++(0,-.4);
\fill[bati] (A)--++(.5,.2) rectangle ++(.2,-.4);
\end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{.3\lw}
\begin{tikzpicture}
\draw[vec,<->] (90:2.5)node[right]{$\vec y_{1}$}--(0,0)--(0:2.5)node[right]{$\vec x_{1}$};
\draw[vec,<->] (110:2.5)node[right]{$\vec y_{2}$}--(0,0)--(20:2.5)node[right]{$\vec x_{2}$};

\node (O) at (0,0)[below]{$\vec z_{1}=\vec z_{2}$};

\draw[->] (0:1.2) arc (0:20:1.2) node[midway,right]{$\alpha$};
\draw[->] (90:1.2) arc (90:110:1.2) node[midway,above]{$\alpha$};
\end{tikzpicture}
\end{minipage}\hfill
\begin{minipage}{.3\lw}
\begin{tikzpicture}
\draw[vec,<->] (90:2.5)node[right]{$\vec x_{2}$}--(0,0)--(0:2.5)node[right]{$\vec z_{2}$};
\draw[vec,<->] (110:2.5)node[right]{$\vec x_{3}$}--(0,0)--(20:2.5)node[right]{$\vec z_{3}$};
\node (O) at (0,0)[below]{$\vec y_{2}=\vec y_{3}$};
\draw[->] (0:1.2) arc (0:20:1.2) node[midway,right]{$\beta$};
\draw[->] (90:1.2) arc (90:110:1.2) node[midway,above]{$\beta$};
\end{tikzpicture}
\end{minipage}
$$O_{1}O_{2}=h,\ O_{2}O_{3}=a,\ O_{3}A_{3}=b, \ovr{O_{1}A_{3}}=h \cdot  \vec z_{1} + a \cdot 	 \vec x_{2} + b \cdot \vec x_{3} \qquad a=b=12\text{ cm}$$
\end{center}

\question{Déterminer $\ovr \Omega_{2/1}$,  $\ovr \Omega_{3/2}$ et $\ovr \Omega_{3/1}$}	

\question{Calculer $\V{A_{3}}(3/1)$ en utilisant les 2 méthodes vues en cours}
Les produits les plus denses vont être concentrés au fond de l'éprouvette, l'accélération "utile", qui produit ce "déplacement", est donc celle qui est dirigée vers le fond de l'éprouvette : $a_{utile} = \ovr{a_{A_{3}}}(3/1) \cdot \vec x_{3}$

\ 

Pour calculer $a_{utile}$, il n'est pas nécessaire de calculer $\ovr{a_{A_{3}}}(3/1)$ puis de projeter. Il est plus rapide d'utiliser la dérivée d'un produit : $(u.v)'=u'.v+v'u$, ce qui donne :

$$
a_{utile}=\left [ \ddt[\V{A_{3}}(3/1) \cdot \vec x_{3}] \right ]_{R_{1}} - \V{A_{3}}(3/1) \cdot \left [ \ddt[\vec x_{3}]  \right]_{R_{1}}
$$




Les produits sanguins sont fragiles et ne doivent pas être soumis à des accélérations trop importantes ( typiquement < 200$\cdot$g). 

Le constructeur indique que la vitesse de rotation de la machine doit être de 850 tr.min$^{-1}$.

Les 2 courbes ci-dessous donnent l'inclinaison de l'éprouvette $\beta$ en fonction de la vitesse de rotation.

\image[.9]{img/graphe1.eps}

\image[.9]{img/graphe2.eps}

\question{Relever l'inclinaison de l'éprouvette pour une vitesse de rotation du rotor de 850 tr/min et en déduire $a_{utile}$. Les produits sanguins sont-ils préservés ?}

D'autres produits moins fragiles peuvent supporter des accélérations bien plus importantes.

\question{Déterminer $a_{utile}$ lorsque le rotor tourne à 10'000 tr/min. Le nombre de "g" indiqué par le constructeur est-il respecté ?}
