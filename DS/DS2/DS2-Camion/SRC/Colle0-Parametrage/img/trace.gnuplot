#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 #mono
set palette cubehelix

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '$\theta (\SI{}{\degree}$'
set ylabel '$d (\SI{}{m})$'

set xrange [0:80]
set xtics 5

plot sqrt()

