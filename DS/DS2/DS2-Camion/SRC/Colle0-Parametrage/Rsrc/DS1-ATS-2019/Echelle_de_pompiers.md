---
titre: Echelle de Pompier
type: DS
classe: ATS
CI: 7 - Transmettre l'énergie
#question-section: 1
#ficheTD: 1
#document: twocolumn,landscape
---



\begin{center}
\begin{minipage}{.47\linewidth}
\textbf{Les parties sont indépendantes}

\medskip

Ce système, conçu et commercialisé par la société CAMIVA, est monté sur le châssis d’un camion de pompiers et permet de déplacer une plate-forme, pouvant recevoir deux personnes et un brancard (charge maxi 270 kg), le plus rapidement possible et en toute sécurité.

\image{img/echelle2.png}

\end{minipage}\hfill
\begin{minipage}{.47\linewidth}
\image{img/echelle1.png}
\end{minipage}
\end{center}





Le système est représenté sous forme de schéma cinématique ci-dessous.

\begin{minipage}{.47\textwidth}
\image{img/echelle3.png}
\end{minipage}\hfill
\begin{minipage}{.47\textwidth}

\image{img/figCalc.pdf}

\medskip

Ce système est constitué de six solides, listés ci-dessous
avec leur repère lié :

\begin{itemize}
\item châssis 0 \dotfill $R_{0}(O,\vec x_{0},\vec y_{0},\vec z_{0})$ ;
\item tourelle 1 \dotfill $R_{1}(A,\vec x_{1},\vec y_{1},\vec z_{1})$ ;
\item berceau 2 \dotfill $R_{2}(A,\vec x_{2},\vec y_{2},\vec z_{2})$ ;
\item parc échelle 3 \dotfill $R_{3}(A,\vec x_{3},\vec y_{3},\vec z_{3})$ ;
\item corps du vérin de dressage 4 \dotfill $R_{4}(B,\vec x_{4},\vec y_{4},\vec z_{4})$ ;

\item tige du vérin de dressage 5 \dotfill $R_{5}(B,\vec x_{5},\vec y_{5},\vec z_{5})$ ;
\item plate-forme 6 \dotfill $R_{6}(G,\vec x_{6},\vec y_{6},\vec z_{6})$.
\end{itemize}

\subsubsection{Paramétrage :}

$\ovr{AB}=a.\vec x_{1} - b . \vec y_{1}$

$\ovr{AD}=L .\vec x_{2}$ ($L$ est une variable)

$\ovr{AC} = l_{2} .\vec x_{2}$

$\ovr{BC}=d.\vec y_{4}$ ($d$ est variable)

\medskip

Quels que soient les mouvements, le système impose à la
\textbf{plate-forme (6) de rester horizontale} grâce à un vérin d'isonivelage entre les pièces \textbf{(6)} et \textbf{(3)} non représenté ici.

\medskip

Autrement dit, \textbf{(6)} est en translation par rapport au sol \textbf{(0)}.


\end{minipage}


\newpage

L’étude se fera pendant la phase de dressage : les
vérins de dressage font pivoter le berceau \textbf{(2)} pendant
que le parc échelle \textbf{(3)} se déploie.

Pendant cette phase :

- le système d’orientation de la tourelle 1 est inactif. **La tourelle \textbf{(1)} est donc solidaire du châssis \textbf{(0)}** ;
- **la vitesse de plate-forme \textbf{(6)} doit-être constante** pour des raisons de confort.



Ce cahier des charges impose, en permanence, des relations entre les valeurs instantanées des paramètres cinématiques.


# Étude du mécanisme

\question{Réaliser le graphe des liaisons.}

# Loi de commande du vérin (4-5)

\question{Par une fermeture géométrique dans le triangle ABC, déterminer une relation entre l'angle $\theta$ et la longueur $d$ du vérin.}

<!--
$$
d^{2} = a^{2} + b^{2} +l_{2}^{2} - 2.l_{2} . \left (a . \cos\theta + b . \sin\theta \right )
$$}
-->

Le tracé de la fonction $d : \theta \mapsto d(\theta)$ est donné ci-dessous:

<!--\image[.8]{img/trace.png}-->

\begin{figure}[htb]
	\centering
	\import{./img/}{plot_dtheta.plot_tex}
	\label{plot_dtheta}
\end{figure}


Le cahier des charges exige que sur l'intervalle correspondant à $\theta \in [30°,70°]$, l'évolution de la grue se fasse à vitesse constante.

Sur la portion étudiée, le tracé de $d$ est assimilable à une droite.

\question{Linéariser (\textit{= déterminer l'équation de la droite}) la fonction $d$ sur l'intervalle $[30°,70°]$.}

\question{Par dérivation de l'équation obtenue, en déduire une relation entre $\dot \theta$ et $\dot d$.

Donner le résultat avec les unités du sytèmes international.}

# Loi de commande du vérin (2-3)

Pour la suite du sujet, on donne $\dot d = K \cdot\dot \theta$  et **on
rappelle que (6) est en translation par rapport au sol (0).**

\question{Montrer que $\V{G,6/0}=\V{D,6/0}$}

\question{Calculer la vitesse $\V{G,6/0}$ en fonction de $\dot \theta$, $\dot L$ et des paramètres géométriques. \\
En déduire $\V{G,6/0}$ en fonction de $\dot d$, $\dot L$ et des paramètres géométriques. }

\question{Calculer l'accélération $\acc{G,6/0}$ en fonction de $L$, $d$ et de leurs dérivées successives.}


\question{En déduire la norme de $\acc{G,6/0}$. Déterminer l'équation entre $L$, $d$ et leurs dérivées successives traduisant que la vitesse
$\V{G,6/0}$ soit constante. Il n'est pas demandé de résoudre cette
équation.}



# Motorisation de la tourelle

On s'intéresse maintenant au mouvement de tourelle \textbf{(2)} par rapport au camion **(1)**.

\begin{figure}[htb]
\centering
\def\svgwidth{\columnwidth}
\import{./img/}{Pompier_3D.pdf_tex}
\label{Chaine_de_transmission_de_puissance_de_la_motorisation_de_la_tourelle}
\end{figure}

On donne

- Nombre de filets de la vis de l'axe moteur : $n_{21}$ = 1
- Nombre de dents de l'arbre intermédiaire : $Z_{23} = 20$ et $Z_{22}$ à déterminer
- Nombre de dents de la roue dentée de l'axe de tourelle : $Z_{1}=70$
- Vitesse nominale du moteur : $N_{mot}=300$ tr/min

\medskip

On souhaite que la tourelle tourne à $1$ tr/min

\question{Déterminer l'expression de la vitesse de rotation $N_1$ de la
tourelle en fonction de la vitesse $N_{mot}$ de l'arbre moteur ;
En déduire le nombre de dents $Z_{22}$ nécessaire pour assurer l'exigence de
vitesse de rotation de la tourelle.}

La loi de commande de l'axe de la tourelle est représenté ci-dessous.

\begin{figure}[htb]
\centering
\import{./img/}{Loi_de_mouvement.pdf_tex}
\end{figure}

L'accélération maximale du moteur mène après réduction à une accélération de la tourelle
$a=5,2\cdot 10^{-2}$ rd.s$^{-2}$

\question{Déterminer le temps d'accélération $t_1$ et le temps $T$ nécessaire pour effectuer un quart de tour.}


