#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6,4 mono

unset key

set loadpath '~/.config/gnuplot-config/latex'
load 'xyborder.cfg'
load 'grid.cfg'

set xlabel '$\theta$ (°)'
set ylabel 'd (m)'

set xrange [0:80]
set yrange [0.5:2.2]
set xtics 5

a=2
#a=1
b=0
#b=.75
l2=1
#l2=.7

d(x)=(a**2+b**2+l2**2+2*l2*(b*sin(x)-a*cos(x)))**.5

plot d(x*pi/180)



