---
#titre: 
type: Corrigé DM1
classe: TSI 1
CI: 7 Transmettre l'énergie
#competence: 
question-section: 1
document: twocolumn,landscape
---


# Étude du mécanisme

\question{
\Begin{center}
	\includegraphics{img/grapheLiaisons}
\End{center}
}

# Loi de commande du vérin

\question{

$$
\begin{aligned}
	\ovr{AB} + \ovr{BC} + \ovr{CA} = \vec 0 \\
	a \cdot \vec x_{1} - b \cdot \vec y_{1} + d \cdot \vec y_{4} - l_{2} \cdot \vec x_{2} = \vec 0
\end{aligned}
$$
	
En projettant respectivement sur $\vec x_{1}$  et $\vec y_{1}$, on obtient : 


$$
\left \lbrace
\begin{aligned}
	a - d \sin (\alpha) - l_{2} \cos (\theta) = 0 \\
	-b + d \cos (\alpha) - l_{2} \sin (\theta) = 0	
\end{aligned} 
\right . 
\text{\qquad Soit : }
\left \lbrace
\begin{aligned}
	d \sin (\alpha) = a - l_{2} \cos (\theta) \\
	d \cos (\alpha) = b + l_{2} \sin (\theta)
\end{aligned} 
\right .
$$

En élevant au carré et en sommant terme à terme, on obtient :
$$
d^{2} \underbrace{\left [\sin^{2} (\alpha) + \cos^{2} (\alpha)\right ]}_{=1}
 = a^{2} +b^{2} + l_{2}^{2} \underbrace{\left [\sin^{2} (\theta) + \cos^{2} (\theta)\right ]}_{=1} + 2 \cdot l_{2} \left ( b \sin (\theta) - a \cos (\theta) \right )
$$

On retrouve bien l'expression demandée.
}

\Begin{multicols}{2}
\question{
	\image[1]{img/traceCorrige}
	
	$d= 0,017 \cdot \theta + 0,72$
	en mètre et en degré
}
\question{$\dot d = 0,017 \cdot \dot \theta$

soit dans le système international (en mètre et en radian) 
$$
\dot d = 0,97 \cdot \dot \theta 
$$
}
\End{multicols}

# Loi de commande du vérin (2-3)

\question{ Le mouvement (6/0) étant une translation,
$$
\V{G,6/0} = \V{D,6/0} 
$$
En utilisant la méthode de dérivation  ($D \in 6$)
$$
\V{D,6/0} =
\ddv[0]{\ovr{AD}}
= \ddv[0]{L \cdot \vec x_{2}} = \dot L \vec x_{2} + L \ddv[0]{\vec x_{2}}
$$

$
\displaystyle \left [ \ddt[\vec x_{2}] \right]_{R_{0}} = \vec x_{2} \wedge \ovr \Omega(0/2) = \vec x_{2} \wedge (- \dot \theta \vec z_{2}) = \dot \theta \vec y_{2}
$

$$\boxed{
\V{G,6/0} = \dot L \cdot \vec x_{2} + L	\ \dot \theta	\cdot \vec y_{2}
}$$


Soit $\boxed{\V{G,6/0} = \dot L \cdot \vec x_{2} + \frac{L}{K} \ \dot d \cdot \vec y_{2}}$
}

\question{
	$$
	\ovr{\Gamma_{G,6/0}}= \left [ \ddt[\V{G,6/0}] \right]_{R_{0}}
	=
	\ddot L \cdot \vec x_{2}
	+ \dot L \cdot \left [ \ddt[\vec x_{2}] \right]_{R_{0}}
	+ \frac{1}{K} \left (
	\dot L \dot d \cdot \vec y_{2} 
	+ L \ddot d \cdot \vec y_{2}
	+ L \dot d \left [ \ddt[\vec y_{2}] \right]_{R_{0}}
	 \right )
	$$
	
$\displaystyle \left [ \ddt[\vec y_{2}] \right]_{R_{0}} = \vec y_{2} \wedge \ovr \Omega(0/2) = \vec y_{2} \wedge (-\dot \theta  \vec z_{2}) = - \dot \theta \vec x_{2} = - \frac{\dot d}{K} \vec x_{2}$

$$\boxed{
	\ovr{\Gamma_{G,6/0}}
	=
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right ) 
	\cdot \vec x_{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )
	\cdot \vec y_{2}
}$$
}

\question{
	$$
	\norm{\ovr{\Gamma_{G,6/0}}}
	=\sqrt{
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right ) ^{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )^{2}
	}
	$$	
	
	
	Pour que la nacelle évolue à vitesse constante, $L$ et $d$ doivent vérifier :
	$$\boxed{
	\sqrt{
	\left (\ddot L - \frac{L \ \dot d ^{2} }{K^{2}}  \right ) ^{2}
	+
	\left ( 2 \frac{\dot L \ \dot d}{K} + \frac{L \  \ddot d }{K} \right )^{2}
	} = 0
	}
	$$
}
