---
titre: DM 1 Échelle Pivotante Automatique à commande Séquentielle
type: DM 1
classe: TSI 1
CI: 7 Transmettre l'énergie
# competence: 
question-section: 1
#document: twocolumn,landscape
---

 
*Les parties sont indépendantes*
 
\Begin{multicols}{2}
Ce système, conçu et commercialisé par la société CAMIVA, est monté sur le châssis d’un camion de pompiers et permet de déplacer une plate-forme, pouvant recevoir deux personnes et un brancard (charge maxi 270 kg), le plus rapidement possible et en toute sécurité.
	 
\image{img/echelle2.png}
	 
\image{img/echelle1.png}
\End{multicols}
 

Le système est représenté sous forme de schéma cinématique ci-dessous.
 
\Begin{multicols}{2}

\image{img/echelle3.png}
\image{img/figCalc.pdf}

\medskip

Ce système est constitué de six solides, listés ci-dessous
avec leur repère lié :

- châssis 0, $R_{0}(O,\vec x_{0},\vec y_{0},\vec z_{0})$ ;
- tourelle 1, $R_{1}(A,\vec x_{1},\vec y_{1},\vec z_{1})$ ;
- berceau 2, $R_{2}(A,\vec x_{2},\vec y_{2},\vec z_{2})$ ;
- parc échelle 3, $R_{3}(A,\vec x_{3},\vec y_{3},\vec z_{3})$ ;
- corps du vérin de dressage 4, $R_{4}(B,\vec x_{4},\vec y_{4},\vec z_{4})$ ;

- tige du vérin de dressage 5, $R_{5}(B,\vec x_{5},\vec y_{5},\vec z_{5})$ ;
- plate-forme 6, $R_{6}(G,\vec x_{6},\vec y_{6},\vec z_{6})$.
	

### Paramétrage :

$\ovr{AB}=a.\vec x_{1} - b . \vec y_{1}$

$\ovr{AD}=L .\vec x_{2}$ ($L$ est une variable)

$\ovr{AC} = l_{2} .\vec x_{2}$

$\ovr{BC}=d.\vec y_{4}$ ($d$ est variable)

\medskip

Quels que soient les mouvements, le système impose à la **plate-forme 6 de rester horizontale**. Autrement dit, 6 est en translation par rapport au sol (0). 

\End{multicols}

\newpage

L’étude se fera pendant la phase de dressage : les
vérins de dressage font pivoter le berceau 2 pendant
que le parc échelle 3 se déploie. 

Pendant cette phase :

- le système d’orientation de la tourelle 1 est inactif. **La tourelle 1 est donc solidaire du châssis 0** ;
- **la vitesse de plate-forme 6 doit-être constante** pour des raisons de confort.



Ce cahier des charges impose, en permanence, des relations entre les valeurs instantanées des paramètres cinématiques.


# Étude du mécanisme

\question{Réaliser le graphe des liaisons.}

# Loi de commande du vérin (4-5)

\question{Par une fermeture géométrique dans le triangle ABC, montrer la relation suivante entre l'angle $\theta$ et la longueur $d$ du vérin.
$$
d^{2} = a^{2} + b^{2} +l_{2}^{2} - 2.l_{2} . \left (a . \cos\theta + b . \sin\theta \right ) 
$$}

Le tracé de la fonction $d : \theta \mapsto d(\theta)$ est donné ci-dessous:

\image[.8]{img/trace.png}

Le cahier des charges exige que sur l'intervalle correspondant à $\theta \in [30°,70°]$, l'évolution de la grue se fasse à vitesse constante.

Sur la portion étudiée, le tracé de $d$ est assimilable à une droite.

\question{Linéariser (\textit{= déterminer l'équation de la droite}) la fonction $d$ sur l'intervalle $[30°,70°]$.}

\question{Par dérivation de l'équation obtenue, en déduire une relation entre $\dot \theta$ et $\dot d$.}

# Loi de commande du vérin (2-3)

Pour la suite du sujet, on donne $\dot d = K \dot \theta$  et **on rappelle que 6 est en translation par rapport au sol 0.**

\question{Calculer la vitesse $\V{G,6/0}$ en fonction de $\dot \theta$, $\dot L$ et des paramètres géométriques. \\
En déduire $\V{G,6/0}$ en fonction de $\dot d$, $\dot L$ et des paramètres géométriques. }

\question{Calculer l'accélération $\ovr{\Gamma_{G,6/0}}$ en fonction de $L$, $d$ et de leurs dérivées successives.}

\question{\textit{Question Bonus} En déduire la norme de $\ovr{\Gamma_{G,6/0}}$. Déterminer une relation entre $L$, $d$ et leurs dérivées successives pour que la vitesse $\V{G}(6/0)$ soit constante.

\ 

Application numérique : $a$=1m ; $b$=0,75m ; $l_{2}$=0,70m}
