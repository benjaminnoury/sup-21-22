#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6.5,4.5 mono
set palette cubehelix

set key top right

# Réglage bordure
set format '\num{%g}'
####

# Réglage Grid
set grid xtics ytics mxtics mytics ls 3 lw 1 lc rgb 'gray'
##

set xlabel 'Temps (ms)'
set ylabel 'Vitesse de rotation (rd/s)'

set xrange [0:3]
set xtics .25
set ytics 2e-5
set mxtics 2
set mytics 5


set samples 1000

A=1
E=8e-5
om=8
m=.25
op=om*sqrt(1-m**2)
phi=acos(m)

e(x)=E
s(x)=A*E*(1-exp(-m*om*x)*sin(op*x+phi)/sqrt(1-m**2))

plot  s(x) t '$\omega(t)$' lw 6, e(x) lw 6 t '$\omega_C(t)$', 



