#!/usr/bin/gnuplot -persist
# vi:syntax=gnuplot 

reset

set terminal cairolatex pdf size 6.5,4 mono
set palette cubehelix

unset key

# Réglage bordure
set format '\num{%g}'
####

# Réglage Grid
set grid xtics ytics mytics mxtics ls 3 lw 1 lc rgb 'gray'
##

set xlabel 'Temps (ms)'
set ylabel 'Vitesse moteur (rad/s)'

#set xtics 1
set	mytics 3
set mxtics 5

E=12
km=25
tau=5

set xrange [0:6*tau]

plot km*E*(1-exp(-x/tau)) lw 6
