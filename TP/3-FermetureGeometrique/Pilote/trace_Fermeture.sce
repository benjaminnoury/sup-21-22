theta=linspace(0,90,1000)*3.1415/90


a=593.5
b=155
R=180

x=sqrt(a*a+b*b+R*R+2*R*(a*cos(theta)-b*sin(theta)))

clf
plot(theta,x)
