from numpy import *
import matplotlib.pyplot as plt

# données géométriques
H=109 # en mm
R=81  # en mm
alpha=44*pi/180
theta_21=linspace(-30,210,150)*pi/180 # 150 points de -30 à 210

## Loi entrée sortie géométrique
theta_31=-arctan((H+R*sin(theta_21))/(R*cos(theta_21)))+alpha+pi/2

for i in range(size(theta_21)):
    if theta_21[i]>pi/2:
        theta_31[i]=theta_31[i]-pi


plt.plot(theta_21*180/pi,theta_31*180/pi,label="Calcul")


if __name__ == "__main__":
    plt.ylabel('theta_31 (°)')
    plt.xlabel('theta_21 (°)')
    plt.title("Loi entrée-sortie de la barriere")
    plt.grid(which="both")
    plt.legend()
    plt.show()

