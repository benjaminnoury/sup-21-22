/////
// Tracé de la loi entrée sortie de la barrière
/////

// données géométrique
H=109   //mm
R=81    //mm
alpha=44*%pi/180 // décalage entre theta_31 et l'angle de lisse
theta_21d=(-30:1:210)'  // position initiale


theta_21=theta_21d*%pi/180 

// loi entrée sortie
theta_31=atan((H+R*sin(theta_21))./(R*cos(theta_21)))-44*%pi/180

// Discontinuité de la fonction arctangente
for i=1:size(theta_21,1)
    if theta_21(i)> %pi/2
        theta_31(i)=theta_31(i)+%pi
    end
end


//figure;clf;
plot(theta_21d,theta_31*180/%pi,'r')

xlabel("Angle manivelle en degré")
ylabel("Angle barrière en degré")

legend('Expérimental','théorique')
zoom_rect([min(theta_21d),-10,max(theta_21d),100])
set(gca(),"grid",[1 1])
