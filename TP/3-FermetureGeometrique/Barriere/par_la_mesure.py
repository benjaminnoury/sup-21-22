from numpy import *
import matplotlib.pyplot as plt
import pandas as pa

data=pa.read_csv('./descente.txt',sep='\s+',header=[1,2]).values
Temps=data[:,0]
theta_31=data[:,1]
Frequence=data[:,2]

#  Calcul de la vitesse de rotation du moteur
# à partir des caractéristiques du moteur.
# (sera expliqué en Spé)

g=.1   # hypothèse : Glissement du moteur constant 10%
R=20   #rapport de transmission du  réducteur moteur
p=2    # nombre de paire de pôles

Wm=(1-g)*2*pi/p*Frequence/R


# Intégration numérique pour obtenir la position de l'arbre moteur

#position initiale -30°
from scipy.integrate import cumtrapz
theta_21=cumtrapz(Wm,Temps,initial=-30*pi/180)


# Tracé de la position
plt.plot(theta_21*180/pi, theta_31, label="Mesure")

if __name__ == "__main__":
    plt.ylabel('theta 31 (°)')
    plt.xlabel("Theta 21 (°)")
    plt.title("Loi entrée-sortie de la barriere")
    plt.grid(which="both")
    plt.legend()
    plt.show()

