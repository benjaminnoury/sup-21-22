import numpy as np
import pandas as pa
import matplotlib.pyplot as plt

import par_le_calcul
import par_la_mesure_sans_panda


plt.ylabel('theta_31 (°)')
plt.xlabel('theta_21 (°)')
plt.title("Loi entrée-sortie de la barriere")
plt.grid(which="both")
plt.legend()
plt.show()


