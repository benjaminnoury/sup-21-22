from numpy import *
import matplotlib.pyplot as plt


# Paramétrage
h=97 # en mm
R=37 # mm


theta_vis=linspace(0,36000,150)*pi/180
theta_30=theta_vis/90 # réducteur

z=R*sin(theta_30)+(h**2-(R*cos(theta_30))**2)**(.5)

plt.plot(theta_vis,z,label="Calcul")

if __name__ == "__main__":
    plt.xlabel('theta_vis (rad)')
    plt.ylabel("z (mm)")
    plt.title("Loi entrée-sortie du Moby-Crea")
    plt.grid(which="both")
    plt.legend()
    plt.show()

