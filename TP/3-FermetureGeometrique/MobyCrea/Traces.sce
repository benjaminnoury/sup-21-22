// Comparaison résultats 

//Paramètres
h=97 //mm
R=37 //mm


theta_vis_d=0:400:36000
theta_vis=theta_vis_d*%pi/180

theta_30=theta_vis/90

z=R*sin(theta_30)+(h**2-(R*cos(theta_30))**2)**(.5)

clf
plot(theta_vis_d,z,'b')

// Simu
S=fscanfMat("simu.csv")


ylabel("z (mm)")
xlabel("angle vis (°)")
legend("théorique","simu","expérimental")
