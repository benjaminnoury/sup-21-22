import matplotlib.pyplot as plt

import par_le_calcul
import par_la_mesure

plt.xlabel('delta (tr)')
plt.ylabel(" alpha (°)")
plt.title("Loi entrée-sortie du Maxpid")
plt.grid(which="both")
plt.legend()
plt.show()

