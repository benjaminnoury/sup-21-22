from numpy import *
import matplotlib.pyplot as plt

#### 1ere ZONE à modifier ####
f = open("Maxpid-Trapeze.txt","r",encoding="latin1")

# Numéro de colonne des différentes données
colonneTemps=0
colonneAlpha=1
coloneVitesseMot=3

#### Fin #####

L = f.readlines()
f.close()

Temps = []
alpha = []
w_mot = []

for k in range(2,len(L)):
    L[k] = ' '.join(L[k].split())
    L[k] = L[k].strip()
    L[k] = L[k].split(" ")
    Temps.append(float(L[k][colonneTemps]))
    alpha.append(float(L[k][colonneAlpha]))
    w_mot.append(float(L[k][coloneVitesseMot]))



#### 2e zone à modifier

Temps = array(Temps)/1000   #ms -> s
alpha = array(alpha)        # °
w_mot = array(w_mot)/2/pi   # tr/s

#### Fin

# intégration de la vitesse moteur
from scipy.integrate import cumtrapz
delta=cumtrapz(w_mot,Temps)-37

# tracé des courbes
plt.plot(delta,alpha,label="Mesure")

if __name__ == "__main__":
    plt.xlabel('delta (tr)')
    plt.ylabel(" alpha (°)")
    plt.title("Loi entrée-sortie du Maxpid")
    plt.grid(which="both")
    plt.legend()
    plt.show()

