from numpy import *
import matplotlib.pyplot as plt

# Vecteur contenant les valeurs de l'angle alpha
alpha=linspace(0,90,100)*pi/180

# Paramétrage
R=82
a=70
b=82
p=4

lambda_min=70
lambda_max=180

#### Zone à éditer

lbda=sqrt(R*R+a*a+2*R*(a*cos(alpha)-b*sin(alpha)))

delta=-lbda/p

#### Fin


plt.plot(delta,alpha*180/pi,label='Calcul')

if __name__ == "__main__":
    plt.xlabel('delta (tr)')
    plt.ylabel(" alpha (°)")
    plt.title("Loi entrée-sortie du Maxpid")
    plt.grid(which="both")
    plt.legend()
    plt.show()

