// Tracé de la loi entrée sortie géométrique du Maxpid obtenue par le calcul.

// Vecteur contenant les valeurs de l'angle alpha   [début intervalle : distance entre les valeurs : fin intervalle]
alpha_deg=[0:1:90];
alpha=alpha_deg*%pi/180;   


//
// Paramètres géométriques
//

R=82
a=70
b=82
p=4

lambda_min=70
lambda_max=180

// Compléter ici avec le résultat de votre calcul. 
// la racine carré s'obtient avec sqrt( )
// la fonction puissance avec ^

lambda= sqrt(R^2 + a^2 +b^2 + 2*R *(a*cos(alpha)-b*sin(alpha)))

delta=-360/p*lambda


clf()			// permet l'effacement de la fenêtre graphique courante

subplot(211)
plot(lambda,alpha_deg,"b") //tracé de la famille de point (alpha(t),lambda(t))
plot(ones(1,size(alpha,2))*lambda_min,alpha_deg,"r") //borne inf de \lambda
plot(ones(1,size(alpha,2))*lambda_max,alpha_deg,"r") //borne sup de \lambda

//title("$\mbox{Loi Entrée/Sortie }\alpha=f(\lambda)$") // titre du graphique
ylabel("$\alpha \mbox{ (en rd)}$") // titre de l'abscisse
xlabel("$\lambda \mbox{ (en mm)}$") // titre de l'ordonnée
subplot(212)
plot((delta-min(delta))/360,alpha_deg,"b") //tracé de la famille de point (alpha(t),lambda(t))



// la fonction cd permet de changer de répertoire.
// Compléter avec le répertoire où sont enregistrée vos mesures.

// La fonction fscanfmat transforme un fichier txt en une matrice scilab
// Adapter en fonction du nom de votre fichier
M=fscanfMat("maxpid trapeze.Txt"); 

// Adapter le numéro des colonnes.
Temps=M(:,1)/1000;
Position=M(:,3)
VitesseMoteur=M(:,4)


n=size(M,1)//renvoie le nombre de ligne de la matrice M


//Calcul de Delta par intégration numérique
delta=0;
for i=2:(n)
    delta(i)=delta(i-1)+ (180*VitesseMoteur(i-1)*(Temps(i)-Temps(i-1))/3.14)
end





//plot2d(delta(:),M(:,2),strf='111',leg='alpha=f(delta)',rect=[mind mina maxd maxa]);//tracé de alpha fonction de delta
plot(delta/360,Position,"r+");//tracé de alpha fonction de delta
xlabel("$\delta \mbox{ (en tr)}$") // titre de l'abscisse
ylabel("$\alpha \mbox{ (en °)}$") // titre de l'ordonnée
legend("Modèle analytique","Expérimental")

